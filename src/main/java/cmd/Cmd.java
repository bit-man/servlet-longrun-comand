package cmd;

import javax.servlet.ServletContext;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Cmd
{
    private final ProcessBuilder pBuilder;
    private final ServletContext ctx;
    private OutputExtractor extractor;
    private List<ISubscriber> sList = new ArrayList<>();

    public Cmd(ServletContext ctx)
    {
        pBuilder = new ProcessBuilder("/bin/ping", "-c", "30", "localhost");
        this.ctx = ctx;
    }


    public void start()
            throws IOException
    {
        extractor = new OutputExtractor(pBuilder.start());
        new Thread(extractor).start();
    }

    public void subscribe(ISubscriber s)
    {

        synchronized (extractor.output)
        {
            for (Character c : extractor.output)
            {
                s.notify(c);
            }

            this.sList.add(s);
        }

    }

    private class OutputExtractor
            implements Runnable
    {
        protected final Process process;
        final private List<Character> output = new LinkedList<>();


        public OutputExtractor(Process process)
        {
            this.process = process;
        }

        @Override
        public void run()
        {
            try
            {
                int c;
                while ((c = process.getInputStream().read()) != -1)
                {
                    synchronized (output)
                    {
                        output.add((char) c);
                        for (ISubscriber s : sList)
                        {
                            s.notify((char) c);
                        }
                    }
                }
            } catch (IOException e)
            {
                ctx.log("Process output display error", e);
            } finally
            {
                for (ISubscriber s : sList)
                {
                    s.endOutput();
                }
            }
        }

    }


}
