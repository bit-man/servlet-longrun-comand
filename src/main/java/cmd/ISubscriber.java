package cmd;

public interface ISubscriber
{
    void notify(char c);

    boolean hasEnded();

    void endOutput();
}