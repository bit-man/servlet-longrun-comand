package cmd;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

public class CmdExecutor
        extends HttpServlet
{

    private final Map<String,Cmd> cmdStore = new HashMap<>();

    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        resp.setContentType("text/plain;charset=UTF-8");
        PrintWriter out = resp.getWriter();

        final HttpSession session = req.getSession();
        final Cmd cmd;

        synchronized (cmdStore)
        {
            if (!cmdStore.containsKey(session.getId()))
            {
                cmd = new Cmd(getServletContext());
                cmd.start();
                cmdStore.put(session.getId(), cmd);
            } else
            {
                cmd = cmdStore.get(session.getId());
            }
        }


        out.println("Session id : " + session.getId());
        printCommandOutput(out, cmd);
        out.println("BYE !!");

        synchronized (cmdStore) {
            cmdStore.remove(session.getId());
        }

        out.close();

    }

    private void printCommandOutput(PrintWriter out, Cmd cmd)
    {
        final CmdOutputDisplay display = new CmdOutputDisplay(out, getServletContext());
        cmd.subscribe(display);

        final Thread thread = new Thread(display);
        thread.start();
        try
        {
            thread.join();
        } catch (InterruptedException e)
        {
            log("INTERRUPTED, signaling end of output display");
        }
    }

}
