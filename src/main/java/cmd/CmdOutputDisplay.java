package cmd;


import javax.servlet.ServletContext;
import java.io.PrintWriter;

public class CmdOutputDisplay
        implements ISubscriber, Runnable
{
    private final PrintWriter out;
    private final ServletContext ctx;
    private boolean end;

    public CmdOutputDisplay(PrintWriter out, ServletContext ctx)
    {
        this.out = out;
        this.ctx = ctx;
        end = false;
    }

    @Override
    public void notify(char c)
    {
        out.print(c);
        out.flush();
    }

    @Override
    public boolean hasEnded()
    {
        return end;
    }

    @Override
    public void endOutput()
    {
        this.end = true;
    }

    @Override
    public void run()
    {
        while( ! hasEnded()) {
            try
            {
                Thread.sleep(500);
            } catch (InterruptedException e)
            {
                endOutput();
                ctx.log("INTERRUPTED, signaling end of output display");
            }
        }

    }
}
